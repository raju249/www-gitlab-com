---
layout: job_family_page
title: "People Experience Associate"
---
<a id="associate-requirements"></a>

### Responsibilities
- Primary responder to people operations email and Slack channel to ensure extraordinary customer service to team members and leaders at GitLab.
- Administer and coordinate all onboarding tasks and provide assistance as needed.
- Administer and coordinate all offboarding tasks and provide assistance as needed.
- Administer anniversary gifts.
- Work closely with People Operations and broader People team to implement specific People Operations processes and transaction.
- Proactively identify process inefficiencies and inconsistencies and collaborate towards an improved and more productive process that improves the team member and/or manager’s experience.
- Announcing changes and improvements on the Company Call and related slack channels.
- Use monthly [Onboarding NPS survey](https://about.gitlab.com/handbook/people-group/#nps-surveys) to report on results from 30 day Onboarding survey for iterations and ideas on improving onboarding for users.
- Administering all transition issues for internal promotions or laterel transfers. Collaborote with the People Analyst team to keep improving the work-flow.
- Handle queries in slack and email relating to different entities and countries.
- Administration of Company communication (company calls, group conversations and AMA's)
- Foster collaborative working relationship with the CES team to ensure a new hire has a positive onboarding experience. Escalate any concerns to Senior People Experience Associate.
- Complete ad-hoc projects, reporting, and tasks.

### Requirements

- The ability to work autonomously and to drive your own performance & development is important in this role.
- Prior extensive experience in a HR or People Operations role.
- Clear understanding of HR laws in one or multiple countries where GitLab is active.
- Willingness to work odd hours when needed (for example, to call an embassy in a different continent).
- Excellent written and verbal communication skills.
- Exceptional customer service skills.
- Strong team player who can jump in and support the team on a variety of topics and tasks.
- Enthusiasm for and broad experience with software tools.
- Proven experience quickly learning new software tools.
- Willing to work with git and GitLab whenever possible.
- Willing to make People Operations as open and transparent as possible.
- Proven organizational skills with high attention to detail and the ability to prioritize.
- You share our [values](/handbook/values), and work in accordance with those values.
- The ability to work in a fast paced environment with strong attention to detail is essential.
- High sense of urgency and accuracy.
- Bachelor's degree or 2 years related experience.
- Experience at a growth-stage tech company is preferred.
- Ability to use GitLab

## Levels

### People Experience Coordinator

- Review contract to ensure signed and PIAA agreement - escalate information as required.
- Audit referral bonus process.
- Announcing changes and improvements on the Company Call and related slack channels.
- Flowers and gift ordering.
- Process all employment confirmation, reference, visa and mortgage letters.
- Audit offboarding issues weekly, to ensure People Group tasks were completed.
- Business card & travel system administration.
- Onboarding Support: New hire invites on team calendars, new hire swag administration and adding new hire's to GitLab Unfiltered YouTube channel.
- Build understanding of GitLab employment practices and provide support as required.
- Manage the Team Meetings calendar.
- Complete ad-hoc projects, reporting, and tasks.

### Senior People Experience Associate

- Support the People Operations Associate with all people operations emails and Slack channel to ensure extraordinary customer service to team members and leaders at GitLab.
- Administer anniversary gifts and audit quarterly for accuracy.
- Escalate any concerns from team members and assist in moving blocks.
- Continiously review feedback accross email, slack and in calls, and partner with the People Experience Associate to continiously improve the onboarding experience.
- Review and provide monthly report on results from 30 day Onboarding survey.
- Administer and coordinate all onboarding tasks and provide assistance as needed.
- Administer and coordinate all offboarding tasks and provide assistance as needed.
- Administer all transition Issues for internal promotions or laterel transfers.
- Partnering with CES and Business Partner to handle more urgent onboarding or offboarding requests to ensure new hire's can attend various events or in cases when it is unusually urgent.
- Handle queries in slack and email relating to different entities and countries.
- Manage the Team Meetings calendar.
- Foster collaborative working relationship with the CES team to ensure a new hire has a positive onboarding experience.
- Collaborate closely with the People Experience Associate and Team Lead to ensure onboarding, offboarding and transition issues can scale with the organisational growth.
- Complete ad-hoc projects, reporting, and tasks.

### Team Lead People Experience

- Coach and mentor People Experience team to effectively represent GitLab's culture and values in all team member interactions
- Bi-weekly People Experience Team meeting to calibrate on weekly priorities.
- Continually audit and monitor compliance with policies and procedures.
- Address any gaps and escalate to Manager, People Operations where required.
- Drive continued automation and efficiency to enhance the employee experience and maintain our efficiency value.
- Announcing changes and improvements on the Company Call and related slack channels.
- Driving a positive team member experience throughout their lifecycle with GitLab.
- Partner with different teams to ensure a team member is fully offboarded.
- Continually audit offboarding process, identify gaps and address.
- Improve transition issues and partner with L&D to implement training.
- Partner with all teams and hiring managers to ensure a team member is fully onboarded.
- Continually audit onboarding process, identify gaps and address through Issue board.
- Quarterly review of 30 day Onboarding survey.
- Support team on escalated queries / issues.
- Complete ad-hoc projects, reporting, and tasks.

## Performance Indicators

Primary performance indicators for this role:

- [Onboarding TSAT > 4](/handbook/people-group/people-operations-metrics/#onboarding-enps)
- [Onboarding task completion < X (TBD)](/handbook/people-group/people-operations-metrics/#onboarding-enps)

### Hiring Process
- Qualified candidates will be invited to schedule a 30 minute screening call with one of our recruiters
- Next, candidates will be invited to schedule a 45 minute interview with our People Operations Manager
- After that, candidates will be invited to schedule a 30 minute interview with members of the People Operations and People Partner teams
- After that, candidates will be invited to interview with the Director of People Operations
- Finally, our CPO may choose to conduct a final interview
