---
layout: handbook-page-toc
title: "The GitLab Tax Team"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# Performance Indicators

The tax department is responsible for GitLab’s overall tax strategy including all components of tax compliance, tax planning and accounting for income taxes. Defining our success as a Tax Team for having an Effective Tax Risk Managament is also measured by our perfomance indicators. 

## Effective Tax Rate (ETR)
The ETR is calculated by dividing the total tax expense by the earnings before tax (EBT). In formula: ETR = Total Tax Expense / EBT. The target percentage is to be determined.

## Budget vs. Actual
Measures the percentage deviation between the actual corporate tax expense and the planned corporate tax expense over the same time period. The time period may be monthly, quarterly, or yearly. In formula: Percentage Deviation = (Actual Expenses -/- Planned Expenses) / planned expenses * 100%. The target percentage is to be determined.

## Audit Adjustments
The financial measurement of tax risk is a typical approach for the Tax Team as these metrics are readily available and impact earnings per share (EPS) and other financial measures. One of the PI's is the frequency and magnitude of audit adjustments. The target amounts are to be determined.

## International Expansion
GitLab has contracted team members in multiple countries. Our aim is to be compliant with local labor laws in all jurisdictions that we operate. In order to mitigate any employment exposure GitLab aims to have a scalable employment solution in place for 100% of its team members. A scalable employment solution would either be hiring through a PEO, hiring through a contractor's entity or a direct contract if permissible by local labor regulations. The percentage is calculated as a percentage of contractors covered by a scalable employment solution in relation to the total workforce.
