---
layout: markdown_page
title: "Command of the Message"
---

## Overview

Force Management's definition of Command of the Message is "being audible ready to define your solutions to customers’ problems in a way that differentiates you from your competitors & allows you to charge a premium for your products & services.” Critical sales skills to demonstrate Command of the Message include:
*  Uncover Customer Needs
*  Articulate Value and Differentiation
*  Negotiate Value

## GitLab Value Framework

| **Value Driver** | **Description** |
| ------ | ------ |
| **Increase Operational Efficiencies** | Simplify the software development toolchain to reduce total cost of ownership |
| **Deliver Better Products Faster** | Accelerate the software delivery process to meet business objectives |
| **Reduce Security and Compliance Risk** | Simplify processes to comply with internal processes, controls, and industry regulations without compromising speed |

## Defensible Differentiators

1.  Single Application for Entire DevOps Lifecycle
1.  Leading SCM and CI in One Application
1.  Built In Security and Compliance
1.  Deploy Your Software Anywhere
1.  Optimized for Kubernetes
1.  End-to-End Insight and Visibility
1.  Flexible GitLab Hosting Options
1.  Rapid Innovation
1.  Open Source; Everyone Can Contribute
1.  Collaborative and Transparent Customer Experience

## Resources: Core Content

| **Asset Title** | **Why / When Use** |
| ------ | ------ |
| [GitLab Value Framework](https://drive.google.com/open?id=1E0QM_dNhAo9bslmtTW1xGqnT9E3kbHBt7UhwGtdYIV8) | primary asset with in-depth information on GitLab's value drivers and defensible differentiators |
| [GitLab Value Framework Summary](https://drive.google.com/open?id=1l2g7OJ3mIrCgUlLvwQbtwo-2Eg1lV8rkqUTy9dEoLdc) | 1-page quick guide, also great for sharing with internal champions |
| [GitLab CXO Digital Transformation Discovery Guide](https://drive.google.com/open?id=1balLINV-vnd6-6TYzF3SIIJKrBPUQofVSDgQ5llC2Do) | 3-page conversation guide for executive discussions |
| [Proof Points](https://drive.google.com/open?id=1Tuhg6LO0e6R-KP7Vfwb8cggHDiBzkC26pBl0lyLS7UY) | don't take our word for it--3rd party validation from customers, analysts, industry awards, and peer reviews<br><br>[Proof Points are also en route to this WIP handbook page](/handbook/sales/command-of-the-message/proof-points.html) |
| [NEW Customer deck based on GitLab value drivers](https://docs.google.com/presentation/d/1SHSmrEs0vE08iqse9ZhEfOQF1UWiAfpWodIE6_fFFLg/edit?usp=sharing) | alternative to the standard GitLab pitch deck that starts with the customer's perspective and aligns to the 3 value drivers. Video [here](https://youtu.be/UdaOZ9vvgXM) |

## Command Plan

*  [Opportunity Management Guidelines](https://about.gitlab.com/handbook/sales/#opportunity-management-guidelines)
*  Command Plan templates
   - [Blank template](https://drive.google.com/open?id=1uTHRRUQx4IP_dXUnsrfG2x9Ti-XpMneX3IDu2v0TZtY)
   - [Template with helper text](https://drive.google.com/open?id=1SH7yfGFda0jsHcM9QmVI9BQO54fuaMujt-aJfFg-EUA)
   - [Sample Command Plan](https://drive.google.com/open?id=1SdfgEds7NvCezcrFjirdLygIp4715iQn7u2ts-9avwg)

## Resources: Additional Job Aids

*  Overview resources
   - Comprehensive [GitLab CoM & MEDDPICC training slide deck](https://drive.google.com/open?id=1bWdV__GwN9WzkidBc0qMFu1GGln3rf5C)
   - [GitLab CoM & MEDDPICC Participant Guide](https://drive.google.com/open?id=1qSn-PZJ9_mk-dhnRY01BdoeBcrtC7jVr)
*  Prepare
   - [Pre-Call Plan](https://docs.google.com/document/d/1yjyfvMoDvayZca5hXiIwSHYc9T1M3mTc7ocqzjhqOf8/copy)
*  Discovery
   - [Customer Call Notes Template](https://docs.google.com/document/d/1hlLvfgQMgQS5g2ykEc6eNZP_wZd1M8GSmS-JsN_vICU/copy)
      - You may also choose to utilize the [Role Play Notes](https://docs.google.com/document/d/185a4mI3HMFnV_l6NwrsAndduFFlTvm5tPiIuPVy0ONQ/copy) template
*  Qualify
   - [Capturing "MEDDPICC" Questions for Deeper Qualification](/handbook/sales/#capturing-meddpicc-questions-for-deeper-qualification)
   - [MEDDPICC training slides](https://drive.google.com/open?id=1i3D64esfBitwn1ZXKB1-yjs52Z5hMsUggVClUKTcqjk)
   - [MEDDPICC template](https://docs.google.com/document/d/1WbHoSL4r7S553n90sAEVuSdBNImWfCk3vTJINw2ud8A/copy)
   - [Opportunity Qualifier](https://docs.google.com/document/d/1Tz6bQKD4Ff2-XqpSXRQslD8yvrphwXaL6oEl74DAjeQ/copy)
*  Role Play materials
   - [Role Play Prep Sheet](https://docs.google.com/document/d/1nQ2yH4hg_btFi5XGHhvDjNh9-TKgxAYGO-bLYl8cMdc/copy)
   - [Role Play Notes](https://docs.google.com/document/d/185a4mI3HMFnV_l6NwrsAndduFFlTvm5tPiIuPVy0ONQ/copy)
*  Sales Manager materials
   - [Opportunity Coaching Guide](https://docs.google.com/document/d/1IZA9Fo2SvZOrtUVpXOjwwqs76lKdXFs4hTezbxRq5v8/copy)
   - [GitLab Manager Coaching slide deck](https://drive.google.com/open?id=1xxWlYd-YoRa51B5AD1LAdl3x5DsXBxfx)
   - [GitLab CoM & MEDDPICC Fast Start Program Manager Playbook](https://drive.google.com/open?id=1n76gU6whKW51ixMfFvgXoGsq512PsCHG)

GitLab sales professionals and Sales Development Reps (SDRs) may access additional information in the [Force Management Command Center](https://gitlab.lyearn.com/) (password protected since resources contain Force Management intellectual property). In particular, the [Channels](https://gitlab.lyearn.com/#/learner/channels) section of the Force Management Command Center contains supplemental instructional videos, podcasts, and blogs).
